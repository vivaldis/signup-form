# Clone repository

`git clone https://bitbucket.org/vivaldis/signup-form.git`

# Build or watch bundles

## For development

### Build CSS

`npm run build:css`

### Build JS

`npm run build:js`

### Build CSS & JS

`npm run build`

### Watch CSS & JS

`npm run watch`

## For production

### Build JS

`npm run prod-build:js`

### Build CSS & JS

`npm run prod-build`

# Start app

Open `index.html` in browser
