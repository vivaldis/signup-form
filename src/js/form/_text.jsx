const React = require('react');

/**
 * props:
 *
 * type string 'text'
 * isValid boolean
 * autofocus boolean
 * noneditable boolean
 * clearable boolean
 * exposable boolean
 * labelText string
 * labelOnClick function
 * textValue string
 * textClassName string
 * textOnChange function
 * placeholder string
 * iconOnClick function
 * hint object
 */
const Text = React.forwardRef((props, outerRef) => {
    let {textValue, textClassName} = {...props};

    // state
    const [inputConfig, setInputConfig] = React.useState({
        value: textValue || '',
        focused: props.autofocus,
        showHint: props.autofocus && !(textValue || '')
    });
    const [inputType, setInputType] = React.useState(props.type || 'text');

    /**
     * inner ref
     * useful when outer ref is not set
     */
    const innerRef = React.createRef();

    function getRefCurrent() {
        return (outerRef || innerRef).current;
    }

    function handleTextChange(e) {
        const [focused, value] = [inputConfig.focused, e.target.value];
        setInputConfig({
            value,
            focused,
            showHint: focused && !value
        });
    }

    function handleTextFocus(e) {
        const {value} = inputConfig;
        setInputConfig({
            value,
            focused: true,
            showHint: !value
        });
    }

    function handleTextBlur(e) {
        setInputConfig({
            ...inputConfig,
            focused: false,
            showHint: false
        });
    }

    // img config
    let img = undefined;
    switch (true) {
        case props.clearable:
            img = {
                src: './assets/img/icon-cancel.png',
                className: 'clear',
                onClick: props.iconOnClick || (() => {
                    getRefCurrent().value = '';
                    setInputConfig({
                        value: '',
                        focused: true,
                        showHint: true
                    });
                })
            };
            break;
        case props.exposable:
            img = {
                src: './assets/img/icon-eye.png',
                className: 'show',
                onClick: props.iconOnClick || (() => {
                    setInputType(inputType === 'password' ? 'text' : 'password');
                })
            };
            break;
        default:
    }

    function naturalFocus(e) {
        const txtElem = getRefCurrent();
        txtElem.focus();
        const curPos = txtElem.selectionStart || 0;
        txtElem.setSelectionRange(curPos, curPos);
    }

    // autofocus
    React.useEffect(() => {
        if (inputConfig.focused) {
            naturalFocus();
        }
    }, [props.autofocus]);

    // change text input class on keystroke if it isn't supplied
    React.useEffect(() => {
        const [txtElem, className] = [getRefCurrent(), 'filled'];
        if ((textClassName === undefined) && txtElem) {
            if (txtElem.value.length) {
                txtElem.classList.add(className);
            } else {
                txtElem.classList.remove(className);
            }
        }
    }, [textValue, inputConfig.value, textClassName]);

    return (
        <div className={`form-control${props.isValid === false ? ' invalid' : ''}`}>
            <label className="form-label" onClick={props.labelOnClick || naturalFocus}>
                {props.labelText}
                <input type={inputType}
                    className={`${(textClassName ? `${textClassName} ` : '')}form-text form-input`}
                    value={textValue || inputConfig.value}
                    {...(props.noneditable ? {readOnly: true, disabled: true} : {})}
                    {...(props.placeholder ? {placeholder: props.placeholder} : {})}
                    onChange={props.textOnChange || handleTextChange}
                    onFocus={handleTextFocus}
                    onBlur={handleTextBlur}
                    ref={outerRef || innerRef} />
                {img
                    ? <img
                        src={img.src}
                        className={`${img.className ? `${img.className} ` : ''}icon`}
                        onClick={img.onClick} />
                    : null}
            </label>
            {props.hint
                ? (
                    <div className="form-hint">
                        <div className={inputConfig.showHint ? 'visible' : null}>
                            {props.hint}
                        </div>
                    </div>
                )
                : null}
        </div>
    );
});

module.exports = Text;
