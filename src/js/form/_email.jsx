const React = require('react');
const Text = require('./_text.jsx');

class Email extends React.Component {
    constructor(props) {
        super(props);

        // initial state
        this.initialState = {
            value: ''
        };
        this.state = this.initialState;

        // binds
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleIconClick = this.handleIconClick.bind(this);
    }

    componentDidUpdate() {
        this.props.onEmailChange(this.state.value, this.validate());
    }

    handleEmailChange(e) {
        this.setState({
            value: e.target.value
        });
    }

    handleIconClick(e) {
        this.setState(this.initialState);
    }

    validate() {
        return /^.+@[^.]+(\..+)*$/.test(this.state.value);
    }

    render() {
        return (
            <Text
                isValid={!this.state.value || this.validate()}
                autofocus={true}
                clearable={true}
                labelText={this.props.label}
                textValue={this.state.value}
                textOnChange={this.handleEmailChange}
                iconOnClick={this.handleIconClick} />
        );
    }
}

module.exports = Email;
