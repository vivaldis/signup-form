require('core-js/stable/array/filter');
require('core-js/stable/array/map');
const React = require('react');
const Text = require('./_text.jsx');

/**
 * props:
 *
 * type string undefined|'compare'
 * validation {equal?: any, pattern: RegExp, description: string}[]
 */
class Password extends React.Component {
    constructor(props) {
        super(props);

        // initial state
        this.state = {
            value: ''
        };

        // binds
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    handlePasswordChange(e) {
        const value = e.target.value;
        this.props.onPasswordChange(value, this.validate(value));
        this.setState({
            value
        });
    }

    validateDetails(forceValue) {
        const value = forceValue || this.state.value;
        return (this.props.validation || []).map(e => ({
            ...e,
            isValid: ('equal' in e) ? (e.equal === value) : e.pattern.test(value)
        }));
    }

    validate(forceValue) {
        return this.validateDetails(forceValue).filter(e => e.isValid !== true).length === 0;
    }

    renderHint() {
        switch (this.props.type) {
            case 'compare':
                return null;
            default:
                return (
                    <React.Fragment>
                        <h4 className="form-hint-header">Требования к паролю</h4>
                        <p>Для защиты ваших данных пароль должен содержать не менее:</p>
                        <ul className="list">
                            {this.validateDetails().map((e, i) => (
                                <li key={i} className={e.isValid ? 'correct' : 'wrong'}>{e.description}</li>
                            ))}
                        </ul>
                    </React.Fragment>
                );
        }
    }

    render() {
        const isValid = !this.state.value || this.validate();
        const {labelText} = this.props;
        return (
            <Text
                type="password"
                isValid={this.props.isValid === false ? false : isValid}
                autofocus={this.props.autofocus}
                exposable={true}
                labelText={isValid ? labelText : (this.props.labelTextInvalid || labelText)}
                textValue={this.state.value}
                textOnChange={this.handlePasswordChange}
                placeholder="Минимум 8 символов"
                hint={isValid ? null : this.renderHint()} />
        );
    }
}

module.exports = Password;
