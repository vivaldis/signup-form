require('core-js/stable/map/index');
const React = require('react');
const Phone = require('./_phone.jsx');
const Sms = require('./_sms.jsx');
const Text = require('./_text.jsx');
const Auxiliary = require('./_auxiliary.jsx');
const SmsAuxiliary = require('./_sms-auxiliary.jsx');
const TextStatic = require('./_text-static.jsx');
const Password = require('./_password.jsx');
const Email = require('./_email.jsx');

class Form extends React.Component {
    constructor(props) {
        super(props);

        // refs
        this.smsComponent = React.createRef();

        // initialize members
        this.init();

        // initial state
        this.initialState = {
            stage: this.constructor.STAGE_PHONE,
            isValid: undefined
        };
        this.state = this.initialState;

        // binds
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.setPhoneNumber = this.setPhoneNumber.bind(this);
        this.setReceivedSms = this.setReceivedSms.bind(this);
        this.goBackToPhoneNumber = this.goBackToPhoneNumber.bind(this);
        this.allowReSendSms = this.allowReSendSms.bind(this);
        this.handleReSendSms = this.handleReSendSms.bind(this);
        this.setPassword = this.setPassword.bind(this);
        this.setPasswordConfirm = this.setPasswordConfirm.bind(this);
        this.setEmail = this.setEmail.bind(this);
        this.skipEmail = this.skipEmail.bind(this);
    }

    static get KEY_PHONE() { return 'phoneNumber'; }
    static get KEY_SMS() { return 'sms'; }
    static get KEY_PASSWORD() { return 'password'; }
    static get KEY_EMAIL() { return 'email'; }
    static get STAGE_PHONE() { return 1; }
    static get STAGE_SMS() { return 1.5; }
    static get STAGE_PASSWORD() { return 2; }
    static get STAGE_EMAIL() { return 3; }
    static get STAGE_FINAL() { return 99; }
    static get TIMEOUT_SMS_RESEND() { return 60 /* seconds */; }

    init() {
        // members
        /**
         * available values for keys (fields/attributes):
         * - true - is valid
         * - false - is invalid
         * - string - error message
         */
        this.validation = new Map([
            [this.constructor.KEY_PHONE, undefined],
            [this.constructor.KEY_SMS, undefined],
            [this.constructor.KEY_PASSWORD, undefined],
            [this.constructor.KEY_EMAIL, undefined]
        ]);
        this.phoneNumber = {
            numeric: undefined,
            formatted: undefined
        };
        this.sms = {
            sent: undefined,
            received: undefined,
            canReSend: true,
            phone: undefined
        };
        this.password = {
            value: undefined,
            isValid: undefined,
            confirm: undefined
        };
        this.email = undefined;

        // initial state
    }

    handleFormSubmit(e) {
        e.preventDefault();
        switch (this.state.stage) {
            case this.constructor.STAGE_PHONE:
                const isPhoneValid = this.validatePhoneNumber();

                // send sms
                if (isPhoneValid && !this.sms.sent) {
                    this.sendSms();
                }

                this.setState({
                    isValid: isPhoneValid,
                    ...(!isPhoneValid || (this.sms.phone !== this.phoneNumber.formatted)
                        ? {} : { stage: this.constructor.STAGE_SMS }
                    )
                });
                break;
            case this.constructor.STAGE_SMS:
                const isSmsValid = this.validateSms();

                if (isSmsValid) {
                    this.sms.canReSend = true;
                }

                this.setState({
                    isValid: isSmsValid,
                    ...(!isSmsValid ? {} : { stage: this.constructor.STAGE_PASSWORD })
                });
                break;
            case this.constructor.STAGE_PASSWORD:
                const isPasswordValid = this.validatePassword();
                this.setState({
                    isValid: isPasswordValid,
                    ...(!isPasswordValid ? {} : { stage: this.constructor.STAGE_EMAIL })
                });
                break;
            case this.constructor.STAGE_EMAIL:
                const isEmailValid = this.validation.get(this.constructor.KEY_EMAIL);
                this.setState({
                    isValid: isEmailValid,
                    ...(!isEmailValid ? {} : { stage: this.constructor.STAGE_FINAL })
                });
                break;
            case this.constructor.STAGE_FINAL:
            default:
                // go to start
                this.init();
                this.setState(this.initialState);
        }
    }

    setPhoneNumber(numeric, formatted) {
        this.phoneNumber = {
            numeric,
            formatted
        };
    }

    validatePhoneNumber() {
        const isValid = /^\d{10}$/.test(this.phoneNumber.numeric);
        this.validation.set(this.constructor.KEY_PHONE, isValid);
        return isValid;
    }

    generateSms() {
        const newSms = Math.floor(Math.random() * 90000) + 10000;
        window.alert(`Your phone: ${this.phoneNumber.formatted}\nYour SMS: ${newSms}`);
        return newSms;
    }

    sendSms() {
        if (this.sms.canReSend) {
            this.sms.sent = String(this.generateSms());
            this.sms.canReSend = false;
            this.sms.phone = this.phoneNumber.formatted;
            setTimeout(() => {
                this.sms.canReSend = true;
            }, this.constructor.TIMEOUT_SMS_RESEND * 1000);
        }
    }

    setReceivedSms(text) {
        this.sms.received = String(text);
    }

    goBackToPhoneNumber() {
        this.sms.received = undefined;
        this.validation.set(this.constructor.KEY_SMS, undefined);
        this.setState(this.initialState);
    }

    validateSms() {
        const {sent, received, phone} = {...this.sms};
        const isValid = sent && (sent === received) && (phone === this.phoneNumber.formatted);
        this.validation.set(this.constructor.KEY_SMS, isValid);
        return isValid;
    }

    allowReSendSms() {
        this.sms.canReSend = true;
        this.setState({
            isValid: this.validatePhoneNumber()
        });
    }

    handleReSendSms(e) {
        const isPhoneValid = this.validatePhoneNumber();
        if (isPhoneValid) {
            this.sms.received = undefined;
            this.sendSms();
            if (this.state.stage === this.constructor.STAGE_SMS) {
                this.smsComponent.current.clearTextInput();
            }
        }
        this.setState({
            stage: isPhoneValid ? this.constructor.STAGE_SMS : this.constructor.STAGE_PHONE,
            isValid: isPhoneValid
        });
    }

    setPassword(value, isValid) {
        this.password = {
            ...this.password,
            value,
            isValid
        };
        this.setState({
            isValid: this.validatePassword()
        });
    }

    setPasswordConfirm(value) {
        this.password.confirm = value;
        this.setState({
            isValid: this.validatePassword()
        });
    }

    validatePassword() {
        const {value, confirm} = this.password;
        const isValid = this.password.isValid && (value === confirm);
        this.validation.set(this.constructor.KEY_PASSWORD, isValid);
        return isValid;
    }

    setEmail(value, isValid) {
        this.email = value;
        this.validation.set(this.constructor.KEY_EMAIL, isValid);
    }

    skipEmail() {
        if (this.state.stage !== this.constructor.STAGE_EMAIL) return;
        this.email = undefined;
        this.setState({
            ...this.initialState,
            stage: this.constructor.STAGE_FINAL
        });
    }

    renderFieldset() {
        switch (this.state.stage) {
            case this.constructor.STAGE_PHONE:
                const formattedPhoneNumber = this.phoneNumber.formatted;
                return (
                    <Phone
                        phoneNumber={formattedPhoneNumber}
                        cursorPosition={formattedPhoneNumber ? formattedPhoneNumber.length - 1 : undefined}
                        isValid={this.validation.get(this.constructor.KEY_PHONE)}
                        onPhoneNumberChange={this.setPhoneNumber} />
                );
            case this.constructor.STAGE_SMS:
                return (
                    <React.Fragment>
                        <Text
                            labelText='Номер телефона или счёта'
                            clearable={true}
                            textValue={this.phoneNumber.formatted}
                            noneditable={true}
                            iconOnClick={this.goBackToPhoneNumber} />
                        <Sms
                            isValid={this.validation.get(this.constructor.KEY_SMS)}
                            onReceivedSmsChange={this.setReceivedSms}
                            ref={this.smsComponent} />
                    </React.Fragment>
                );
            case this.constructor.STAGE_PASSWORD:
                const isValid = !this.password.confirm || this.validation.get(this.constructor.KEY_PASSWORD);
                return (
                    <React.Fragment>
                        <TextStatic
                            label="Номер телефона или счёта"
                            text={this.phoneNumber.formatted} />
                        <Password
                            isValid={isValid}
                            autofocus={true}
                            labelText={!isValid && this.password.isValid
                                ? 'Пароли не совпадают' : 'Придумайте пароль'}
                            labelTextInvalid="Ненадёжный пароль"
                            onPasswordChange={this.setPassword}
                            validation={[{
                                pattern: /\d/,
                                description: 'одной цифры'
                            }, {
                                pattern: /[A-Z]/,
                                description: 'одной большой буквы латиницей'
                            }, {
                                pattern: /[a-z]/,
                                description: 'одной маленькой буквы латиницей'
                            }, {
                                pattern: /.{8,}/,
                                description: '8-ми символов'
                            }]} />
                        <Password
                            type="compare"
                            labelText="Повторите пароль"
                            labelTextInvalid="Пароли не совпадают"
                            onPasswordChange={this.setPasswordConfirm}
                            validation={[{ equal: this.password.value }]} />
                    </React.Fragment>
                );
            case this.constructor.STAGE_EMAIL:
                return (
                    <React.Fragment>
                        <TextStatic
                            label="Номер телефона или счёта"
                            text={this.phoneNumber.formatted} />
                        <Email
                            label="Введите email"
                            onEmailChange={this.setEmail} />
                    </React.Fragment>
                );
            case this.constructor.STAGE_FINAL:
                return (
                    <React.Fragment>
                        <TextStatic
                            label="Номер телефона или счёта"
                            text={this.phoneNumber.formatted} />
                        <TextStatic
                            label="Пароль"
                            text={this.password.value} />
                        <TextStatic
                            label="Email"
                            text={this.email || 'Не задано'} />
                    </React.Fragment>
                );
            default:
                return null;
        }
    }

    renderAuxiliary() {
        switch (this.state.stage) {
            case this.constructor.STAGE_PHONE:
            case this.constructor.STAGE_SMS:
                return (
                    <Auxiliary>
                        {this.sms.canReSend
                            ? (this.sms.sent && this.sms.phone
                                ? (
                                    <button type="button" className="link" onClick={this.handleReSendSms}>
                                        Запросить код повторно
                                    </button>
                                )
                                : null
                            )
                            : (
                                <SmsAuxiliary
                                    seconds={this.constructor.TIMEOUT_SMS_RESEND}
                                    onTimeOut={this.allowReSendSms} />
                            )
                        }
                    </Auxiliary>
                );
            case this.constructor.STAGE_EMAIL:
                return (
                    <Auxiliary>
                        Укажите свой email, если хотите, чтобы мы присылали вам письма об акциях и новых местах
                    </Auxiliary>
                );
            default:
                return null;
        }
    }

    renderButton() {
        let caption;
        switch (this.state.stage) {
            case this.constructor.STAGE_PHONE:
            case this.constructor.STAGE_SMS:
                caption = 'Продолжить';
                break;
            case this.constructor.STAGE_PASSWORD:
                caption = 'Сохранить';
                break;
            case this.constructor.STAGE_EMAIL:
                caption = 'Сохранить и войти';
                break;
            case this.constructor.STAGE_FINAL:
                caption = 'Всё заново';
                break;
            default:
                return null;
        }
        return (
            <button type="submit" className="button action">
                {caption}
            </button>
        );
    }

    render() {
        return (
            <form onSubmit={this.handleFormSubmit}>
                <header>
                    <h1>Вход в Plus24</h1>
                </header>
                <fieldset>
                    {this.renderFieldset()}
                </fieldset>
                <section>
                    {this.renderAuxiliary()}
                </section>
                <footer>
                    {this.state.stage === this.constructor.STAGE_EMAIL
                        ? (
                            <button type="button" className="button" onClick={this.skipEmail}>
                                Пропустить
                            </button>
                        )
                        : null
                    }
                    {this.renderButton()}
                </footer>
            </form>
        );
    }
}

module.exports = Form;
