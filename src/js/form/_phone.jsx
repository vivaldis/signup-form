require('core-js/stable/array/filter');
require('core-js/stable/array/map');
const React = require ('react');
const Text = require('./_text.jsx');

class Phone extends React.Component {
    constructor(props) {
        super(props);

        // deal with phone mask
        const [mask, maskchar] = [this.constructor.MASK, this.constructor.MASK_CHAR];
        this.digitPositions = [...mask].map((e, i) => i).filter(index => mask[index] === maskchar);
        this.maskPrefix = (mask.match(/\d+/) || [''])[0];
        this.initialMasked = mask.replace(new RegExp(`\\${maskchar}`, 'g'), this.constructor.MASK_PLACEHOLDER);
        this.initialMaskedCursorPosition = this.digitPositions[0];

        // refs
        this.textInputPhone = React.createRef();

        // initial state
        this.initialState = {
            phone: this.props.phoneNumber || this.initialMasked,
            cursorPosition: this.props.cursorPosition || this.initialMaskedCursorPosition
        };
        this.state = this.initialState;

        // binds
        this.handlePhoneChange = this.handlePhoneChange.bind(this);
        this.handleIconClick = this.handleIconClick.bind(this);
        this.handleLabelClick = this.handleLabelClick.bind(this);
    }

    static get MASK() { return '+7 (???) ???-??-??'; }
    static get MASK_CHAR() { return '?'; }
    static get MASK_PLACEHOLDER() { return '_'; }

    componentDidMount() {
        this.setCursor();
    }

    componentDidUpdate() {
        this.setCursor();
        // pass phone up to the form
        const phoneFormatted = this.state.phone;
        this.props.onPhoneNumberChange(this.normalizePhone(phoneFormatted), phoneFormatted);
    }

    setCursor() {
        this.textInputPhone.current.focus();

        let pos = this.state.cursorPosition;
        this.textInputPhone.current.setSelectionRange(pos, pos);
    }

    normalizePhone(rawPhone, dropMaskPrefix = true) {
        const normalized = rawPhone.replace(/\D/g, '');
        return dropMaskPrefix && this.maskPrefix ? normalized.replace(this.maskPrefix, '') : normalized;
    }

    formatPhone(numericPhone) {
        const [digitPositions, formatted] = [this.digitPositions, [...this.initialMasked]];
        for (let i = 0; i < numericPhone.length; i++) {
            formatted[digitPositions[i]] = numericPhone.charAt(i);
        }
        return formatted.join('');
    }

    nextCursorPosition(rawPosition) {
        const digitPositions = this.digitPositions;
        for (let i = 0; i < digitPositions.length; i++) {
            if (rawPosition <= digitPositions[i]) {
                return digitPositions[i];
            }
        }
        return digitPositions[digitPositions.length - 1];
    }

    prevCursorPosition(rawPosition) {
        const digitPositions = this.digitPositions;
        if (digitPositions.indexOf(rawPosition) !== -1) {
            return rawPosition;
        }
        for (let i = digitPositions.length - 1; i >= 0; i--) {
            if (rawPosition >= digitPositions[i]) {
                return digitPositions[i];
            }
        }
        return digitPositions[0];
    }

    handlePhoneChange(e) {
        const [newValueRaw, curValueRaw] = [e.target.value, this.state.phone];
        const [newValueNumeric, curValueNumeric] = [
            this.normalizePhone(newValueRaw), this.normalizePhone(curValueRaw)
        ];
        const isTyped = newValueRaw.length > curValueRaw.length; // some character typed
        const isDeleted = newValueNumeric.length <= curValueNumeric.length; // backspace or delete pressed
        const isNonDigitTyped = isTyped && isDeleted; // some non-digit typed
        const newPositionRaw = e.target.selectionStart - (isNonDigitTyped ? 1 : 0);
        const isOutOfDigitPositions = this.digitPositions.indexOf(newPositionRaw - 1) === -1;
        const newPosition = isDeleted
            ? this.prevCursorPosition(newPositionRaw)
            : this.nextCursorPosition(newPositionRaw);
        const newPhone = isDeleted && !isTyped
            ? this.normalizePhone(curValueRaw.substr(0, newPosition) + curValueRaw.substr(newPosition + 1))
            : newValueNumeric;
        this.setState({
            phone: this.formatPhone(newPhone),
            cursorPosition: newPosition + (isTyped && !isDeleted && isOutOfDigitPositions ? 1 : 0)
        });
    }

    handleIconClick(e) {
        this.setState({
            phone: this.initialMasked,
            cursorPosition: this.initialMaskedCursorPosition
        });
    }

    handleLabelClick(e) {
        this.setCursor();
    }

    render() {
        const isValid = this.props.isValid;
        const phone = this.state.phone;
        return (
            <Text
                isValid={isValid}
                labelText={isValid === false
                    ? 'Введите номер телефона или счёта'
                    : 'Номер телефона или счёта'
                }
                labelOnClick={this.handleLabelClick}
                textValue={phone}
                textClassName={phone !== this.initialMasked ? 'filled' : null}
                textOnChange={this.handlePhoneChange}
                ref={this.textInputPhone}
                clearable={true}
                iconOnClick={this.handleIconClick} />
        );
    }
}

module.exports = Phone;
