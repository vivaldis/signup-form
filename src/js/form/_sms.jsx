const React = require ('react');
const Text = require('./_text.jsx');

class Sms extends React.Component {
    constructor(props) {
        super(props);

        // refs
        this.textInputSms = React.createRef();

        // initial state
        this.state = {
            sms: undefined
        };

        // binds
        this.handleSmsChange = this.handleSmsChange.bind(this);
        this.handleIconClick = this.handleIconClick.bind(this);
    }

    componentDidUpdate() {
        this.props.onReceivedSmsChange(this.state.sms);
    }

    clearTextInput() {
        this.textInputSms.current.focus();
        this.handleIconClick();
    }

    handleSmsChange(e) {
        this.setState({
            sms: e.target.value
        });
    }

    handleIconClick(e) {
        this.setState({
            sms: ''
        });
    }

    render() {
        return (
            <Text
                isValid={this.props.isValid}
                autofocus={true}
                labelText="Введите код из SMS"
                clearable={true}
                textValue={this.state.sms}
                textOnChange={this.handleSmsChange}
                iconOnClick={this.handleIconClick}
                ref={this.textInputSms} />
        );
    }
}

module.exports = Sms;
