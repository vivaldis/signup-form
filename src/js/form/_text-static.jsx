const React = require('react');

class TextStatic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="form-control">
                <div className="form-label">
                    {this.props.label}
                    <span className="form-text">{this.props.text}</span>
                </div>
            </div>
        );
    }
}

module.exports = TextStatic;
