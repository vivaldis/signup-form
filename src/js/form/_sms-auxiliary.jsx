const React = require('react');

class SmsAuxiliary extends React.Component {
    constructor(props) {
        super(props);

        this.interval = 222 /* ms */;
        this.timerId = null;
        this.timeout = (this.props.seconds || 0) * 1000 /* ms */;

        this.state = {
            ms: this.timeout
        };
    }

    componentDidMount() {
        this.timerId = setInterval(() => {
            this.timeout -= this.interval;
            this.setState({
                ms: this.timeout
            });
        }, this.interval);
    }

    componentDidUpdate() {
        if (this.timeout < 0) {
            clearInterval(this.timerId);
            this.props.onTimeOut();
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    getSeconds() {
        return Math.floor(this.state.ms / 1000);
    }

    render() {
        return this.timeout < 0
            ? null
            : (
                <React.Fragment>
                    Запросить код повторно можно через {this.getSeconds()} секунд
                </React.Fragment>
            );
    }
}

module.exports = SmsAuxiliary;
