const React = require('react');

class Auxiliary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return !this.props.children
            ? null
            : (
                <small className="auxiliary">
                    {this.props.children}
                </small>
            );
    }
}

module.exports = Auxiliary;
