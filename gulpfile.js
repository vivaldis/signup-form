const { src, dest, parallel, series, watch } = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const buffer = require('gulp-buffer');
const rename = require('gulp-rename');
const gulpif = require('gulp-if');
const log = require('gulplog');
const browserify = require('browserify');
const watchify = require('watchify');
const source = require('vinyl-source-stream');
const del = require('del');

sass.compiler = require('dart-sass');

function purgeDir(dir = './dist') {
    return () => {
        console.log(`Purging ${dir} directory...`);
        return del([`${dir}/**`, `!${dir}`]);
    }
}

function compileCss() {
    const isProd = process.env.NODE_ENV === 'production';
    console.log(`Compiling${isProd ? ' & minimizing' : ''} css...`);
    return src('./src/scss/main.scss')
        // compile
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(rename({
            basename: 'bundle'
        }))
        // minify
        .pipe(gulpif(isProd, sass({
            outputStyle: 'compressed'
        })))
        .pipe(dest('./dist/css'));
}

/**
 * Bundles, transpiles and compresses JS
 * @param {boolean} isWatch true for watch mode, false for common bundle
 * @returns {NodeJS.ReadWriteStream}
 */
function transpileJs(isWatch = false) {
    const isProd = process.env.NODE_ENV === 'production';
    console.log(
        `[${isProd ? 'prod' : 'dev'}]`,
        `(${isWatch ? 'watch' : 'build'} mode)`,
        'Bundling, transpiling & minimizing js...'
    );

    const [entryScript, outFile, outDir] = ['./src/js/main.jsx', 'bundle.js', './dist/js'];

    const b = (
        isWatch
            ? watchify(browserify({
                ...watchify.args,
                ...{
                    entries: [entryScript],
                    debug: true
                }
            }))
            : browserify(entryScript)
        )
        .transform('babelify', {
            presets: ['@babel/env', '@babel/preset-react'] // @babel/plugin-proposal-class-properties
        })
        .transform('envify', {
            global: true,
            _: 'purge'
        }, {
            global: true
        })
        .transform('uglifyify', {
            global: isProd
        });
    const bundle = () => bundleBrowserify(b, outFile, outDir, isProd, isWatch);
    // watch for watchify
    if (isWatch) {
        b.on('log', log.info);
        b.on('update', bundle);
    }
    // build browserify
    return bundle();
}

/**
 * Makes a bundle for the specified browserify object
 * @param {browserify.BrowserifyObject} objBrowserify browserify object to bundle
 * @param {string} outFile output file name
 * @param {string} outDir output directory
 * @param {boolean} isProd whether to bundle for production environment
 * @param {boolean} isWatch whether to bundle for watch mode
 * @returns {NodeJS.ReadWriteStream}
 */
function bundleBrowserify(objBrowserify, outFile, outDir, isProd = false, isWatch = false) {
    return objBrowserify
        .bundle()
        .pipe(source(outFile))
        .pipe(buffer())
        .pipe(gulpif(!isWatch, uglify({
            compress: isProd,
            mangle: isProd
        })))
        .pipe(dest(outDir));
}

function transpileJsForBuild() {
    return transpileJs();
}

function transpileJsForWatch() {
    return transpileJs(true);
}

const buildCss = series(
    purgeDir('./dist/css'),
    compileCss
);

const buildJs = series(
    purgeDir('./dist/js'),
    transpileJsForBuild
);

// build tasks
exports.css = buildCss;
exports.js = buildJs;
exports.build = parallel(
    buildCss,
    buildJs
);

const watchCss = () => {
    watch('./src/scss/**/*.scss', compileCss);
}

const watchJs = () => {
    watch(['./src/js/**/*.js', './src/js/**/*.jsx'], transpileJsForWatch);
}

// watch tasks
exports.watchCss = series(
    buildCss,
    watchCss
);

exports.watch = series(
    parallel(
        buildCss,
        purgeDir('./dist/js')
    ),
    parallel(
        watchCss,
        transpileJsForWatch
    )
);
